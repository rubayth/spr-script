
const buildAuto = require('./src/BuildAuto');
const buildCustom = require('./src/BuildCustom');
const rl = require('readline-sync');
const fs = require('fs');
const _ = require('lodash');
var path = require('path');


var folderName = "";
var spreadSheetName = "";
var action = "";

if (!process.argv[3] || !process.argv[2]) {
    const folders = fs.readdirSync('./').map(file => {
        const name = fs.statSync(file);
        if (name.isDirectory()) return file
        return null;
    });
    const filteredFolders = _.filter(folders, (name) => {
        if (name !== null && name !== 'node_modules' && name !== 'src' && name !== '.git') return true;
    })


    let folderNum = rl.keyInSelect(filteredFolders, "SELECT EMAIL FOLDER: ");
    const filesInFolder = fs.readdirSync(`./${filteredFolders[folderNum]}`).map(file => {
        return file;
    });
    const xlsxFiles = filesInFolder.filter(function (file) {
        return path.extname(file).toLowerCase() === '.xlsx';
    });
    let xlsxNum = rl.keyInSelect(xlsxFiles, "SELECT SPEADSHEET: ");

    const actionList = ['both', 'auto', 'custom'];
    let actionNum = rl.keyInSelect(actionList, "SELECT ACTION: ");

    folderName = filteredFolders[folderNum];
    spreadSheetName = /([^.]+)/g.exec(xlsxFiles[xlsxNum])[0];
    action = actionList[actionNum];
}

else {
    folderName = process.argv[2];
    spreadSheetName = process.argv[3];
    action = process.argv[4];
}


if (action === 'auto') buildAuto.build(folderName, spreadSheetName);

else if (action === 'custom') buildCustom.build(folderName, spreadSheetName);
else {
    buildAuto.build(folderName, spreadSheetName);
    buildCustom.build(folderName, spreadSheetName);
}

