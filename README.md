# SPR Node Script

Generate Auto or Custom SPR Emails!

## Installation
```sh
$ git clone git@gitlab.com:rubayth/spr-script.git
$ cd spr-script
$ npm install 
```

## Usage
### Step 1 
  - Move SPR Email Folder (SPR_166_100319_CA) inside spr-script folder. You can have multiple SPR Email Folders inside.
  - Do not move folder contents (PSD, excel) outside its folder. Keep it how it is.
  - Directory should look like this: 
```
package-lock.json
index.js
package.json
100119_CA_Canada            folder contents => (.psd, .jpg, .xlsx)     
node_modules
SPR_166_100319_CA           folder contents => (.psd, .jpg, .xlsx)
src
```
### Step 2 - PSD
  - Slice PSD file, making sure rows go all the way through 
  - No need to edit slice options and input alt text or href
  - After slicing, save for web legacy and save images only with filename "image". 
  - (Will generate images with image_sliceNumber.jpg naming convention)

### Step 3 - Excel
  - Edit config.js file in src folder to match columns in spreadsheet
    -- Default Config:
```
module.exports = {
    config: {
        sku: "C",
        alt: "D",
        url: "M",
        slices: "O",
        custom : "P",
        bgColor: "Q",
        fontColor: "R",

    }
}
```
  - Column O: input comma seperated numbers correlating to the slice numbers.
  - CUSTOM ONLY:
    - Column P: input slice number containing the price (custom)
    - Column Q: Background color of the custom slice (Leave empty if white)
    - Column R: Font color of custom value. (Leave empty if black)
  - After last row, input values for footer on Column O, P, Q, R

### Example SpreadSheet: (Row #5 is footer)
| Row # | C | D | M | N | O | P | Q | R |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | Full SKU | Description | CONCATENATE |  | SLICES | CUSTOM SLICE | BG COLOR | FONT COLOR |
| 2 | RAC84251 | Lemon/Lime Disinfecting Wipes | /sl-forward-sprisv/RAC/84251/1022221859/668-851 |  | 2, 3, 5, 7, 8, 10 | 8 | e3e5a7 | TRUE |
| 3 |RAC85017|Hydrogen Peroxide Multi-Purpose Cleaner|/sl-forward-sprisv/RAC/85017/1024721485/769-775| |4, 6, 9, 11, 9	|9|
| 4 ||KND25954|Dark Chocolate Cocoa Bars|/sl-forward-sprisv/KND/25954/1040919837/658-806||46, 47, 49, 51, 53|51|49def4
| 5 |||||54|54|5a6265|TRUE

### Step 4 - Run
```sh
$ npm start
OR 
$ node index.js <SPR-email-folder-name> <SpreadSheet-Name> <auto || custom || empty for both>
Example:
$ node index.js   SPR_166_100319_CA   DEV_ONLY_Oct03-CA  
```
##### Outputs a new folder inside project folder labled AUTO and/or CUSTOM


### Troubleshoot
  - Error on start
    - Make sure the spr folder with the spreadsheet, images, PSD is in the directory
  - Missing slice #0 error
    - Looking for trailing comma in spreadsheet and remove it
  - Missing slice #(number) error
    - Either a duplicate slice or missing slice. Will output slice information in JSON before and afer error for reference.
    - If outputed JSON contains NaN values, make sure config.js is setup properly.


