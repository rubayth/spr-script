const excelToJson = require("convert-excel-to-json");
const mkdirp = require("mkdirp");
const fs = require("fs-extra");
const _ = require("lodash");
const sizeOf = require('image-size');

const { config } = require('./config');

const build = (folderName, spreadSheetName) => {
    const result = excelToJson({
        sourceFile: `./${folderName}/${spreadSheetName}.xlsx`,
        header: {
            rows: 1
        }
    });

    const data = _.map(result['SKU Information'], row => {
        return _.map(_.split(row[config.slices], ','), num => {
            let imageNum = _.trim(num);
            let custom = false;
            if (imageNum == row[config.custom]) {
                let bgColor = 'FFFFFF';
                let fontColor = '020203';
                if (row[config.bgColor]) bgColor = row[config.bgColor];
                if (row[config.fontColor]) fontColor = row[config.fontColor];
                custom = {
                    bgColor,
                    fontColor
                };
            }
            return {
                num: Number(imageNum),
                sku: row[config.sku],
                alt: row[config.alt],
                url: row[config.url],
                custom
            }
        });
    })

    const customFolderName = folderName + "_CUSTOM";
    const sortedData = _.sortBy(_.flatMapDeep(data), obj => obj.num);

    for(var i = 1; i < sortedData.length; i++) {
        if(sortedData[i].num - sortedData[i-1].num != 1) {
               console.error("\nERROR: Slice #" + (sortedData[i-1].num + 1) + " is missing or entered twice.\n");
               console.log(sortedData[i], sortedData[i-1])
               process.exit();
        }
    }

    mkdirp(`./${folderName}/${customFolderName}`, err => {
        if (err) console.error(err);
        else {
            fs.writeFile(`./${folderName}/${customFolderName}/layout1.html`, buildHtml(sortedData, folderName), err => {
                if (err) console.log(`Build error`, err);
                fs.copy(`${folderName}/images`, `./${folderName}/${customFolderName}/images`)
                console.log(`${customFolderName} GENERATED`)
            });
        }
    });
}

const buildHtml = (sortedData, folderName) => {
    const html =
        `<table width="100%" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border-collapse:collapse;" valign="top">
                <table bgcolor="#FFFFFF" align="center" width="580" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="border-collapse:collapse;" valign="top">
                            <a href="*$website_P01$*" sllabel="Website"><img
                                    style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0; margin: 0;"
                                    alt="Company Logo" height="70" src="${folderName}/images/spacer.png"
                                    style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0px;width:580px;height:70px;"
                                    slcomponent="photo1_CUS" sllabel="Logo_580x70" title="photo1_CUS" width="580" />
                            </a>
                        </td>
                    </tr>
                </table>
                <table align="center" bgcolor="#FFFFFF" width="580" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        ${checkData(sortedData, folderName)}
            </table>
        </td>
    </tr>
</table>
    `;
    return html;
};

const checkData = (sortedData, folderName) => {
    let width = 0;
    let count = 0;

    let htmlString = "";
    _.forEach(sortedData, data => {
        let imageNum = data.num;
        if (imageNum < 10) imageNum = '0' + imageNum;

        const imageSrc = `./${folderName}/images/image_${imageNum}.jpg`
        const dimensions = sizeOf(imageSrc);
        width += dimensions.width;

        if (imageNum === sortedData[sortedData.length - 1].num) {
            htmlString += `${buildFooter(data.custom)}`
        }
        else if (width >= 580) {
            count = count + 1;
            width = 0;
            htmlString += ` ${buildTableData(data, dimensions, imageNum)}
            </tr> 
            </table>
            <table align="center" bgcolor="#FFFFFF" width="580" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    `
        }
        else {
            count = count + 1;
            htmlString += `${buildTableData(data, dimensions, imageNum)}`
        }
    });
    return htmlString
}

let customCount = 0;
const buildTableData = ({ sku, alt, url, custom }, dimensions, imageNum) => {
    if (custom) {
        customCount = customCount + 1;
        return `<td style="border-collapse:collapse; background-color: #${custom.bgColor} !important" valign="center"  width="${dimensions.width}" height="${dimensions.height}"> 
                    <a href="${url}" style="text-decoration:none">
                        <span style="font-weight:bold;font-family:sans-serif;">
                            <font face="Arial, Helvetica" size="5" color="#${custom.fontColor}">
                                <b>$</b><b sllabel="${sku}" slcomponent="linetext${customCount}_CUS"></b>
                            </font></span>
                    </a>
                </td>`
    }
    return `<td style="border-collapse:collapse;" valign="top"> 
                <a href="${url}">
                    <img style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0; margin: 0;" src="images/image_${imageNum}.jpg" width="${dimensions.width}" height="${dimensions.height}" border="0" alt="${alt}">
                </a>
            </td>
        `
}

const buildFooter = ({ bgColor, fontColor }) => {
    customCount = customCount + 1;
    return `<td style="border-collapse:collapse; background-color: #${bgColor} !important" valign="top"> 
                <center style="padding: 30px 20px;">
                    <font style="font-family:Arial, Helvetica, sans-serif;font-size:16px;text-align:center;line-height:23px;font-weight:bold;" size="5" color="#${fontColor}">
                        All offers valid now through close of business day
                    </font>
                    <span style="font-weight:bold;font-family:sans-serif;color:#${fontColor} !important;">
                        <font face="Arial, Helvetica" size="4">
                            <b sllabel="Footer Info" slcomponent="linetext${customCount}_CUS">
                            </b>
                        </font>
                    </span><br />
                    <font style="font-family:Arial, Helvetica, sans-serif;color:#${fontColor} !important;font-size:15px;text-align:center;line-height:20px;font-weight:300;" size="4" color="#${fontColor}">Click on products for additional information. While supplies last.</font><br /><font style="font-family:Arial, Helvetica, sans-serif;color:#${fontColor} !important;font-size:13px;text-align:center;line-height:17px;font-weight:300;" size="3" color="#${fontColor}">
                        <br />Not responsible for typographical errors. Subject to price and availability changes.</font>
                </center>
            </td>
        </tr> 
            `
}



module.exports = { build };