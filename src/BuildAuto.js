const excelToJson = require("convert-excel-to-json");
const mkdirp = require("mkdirp");
const fs = require("fs-extra");
const _ = require("lodash");
const sizeOf = require('image-size');

const { config } = require('./config')

const build = (folderName, spreadSheetName) => {
    const result = excelToJson({
        sourceFile: `./${folderName}/${spreadSheetName}.xlsx`,
        header: {
            rows: 1
        }
    });

    const data = _.map(result['SKU Information'], row => {
        return _.map(_.split(row[config.slices], ','), num => {
            let imageNum = _.trim(num);
            return {
                num: Number(imageNum),
                sku: row[config.sku],
                alt: row[config.alt],
                url: row[config.url],
            }
        });
    });
    const autoFolderName = folderName + "_AUTO";
    const sortedData = _.sortBy(_.flatMapDeep(data), obj => obj.num);

    for(var i = 1; i < sortedData.length; i++) {
        if(sortedData[i].num - sortedData[i-1].num != 1) {
               console.error("\nERROR: Slice #" + (sortedData[i-1].num + 1) + " is missing or entered twice.\n");
               console.log(sortedData[i], sortedData[i-1])
               process.exit();
        }
    }

    mkdirp(`./${folderName}/${autoFolderName}`, err => {
        if (err) console.error(err);
        else {
            fs.writeFile(`./${folderName}/${autoFolderName}/layout1.html`, buildHtml(sortedData, folderName), err => {
                if (err) console.log(`Build error`, err);
                fs.copy(`${folderName}/images`, `./${folderName}/${autoFolderName}/images`)
                console.log(`${autoFolderName} GENERATED`)
            });
        }
    });
}

const buildHtml = (sortedData, folderName) => {
    const html = `<table width="100%" bgcolor="#363636" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="border-collapse:collapse;" valign="top">
                            <table bgcolor="#FFFFFF" align="center" width="580" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="border-collapse:collapse;" valign="top">
                                        <a href="*$website_P01$*" sllabel="Website"><img
                                                style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0; margin: 0;"
                                                alt="Company Logo" height="70" src="./${folderName}/images/spacer.png"
                                                style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0px;width:580px;height:70px;"
                                                slcomponent="photo1_CUS" sllabel="Logo_580x70" title="photo1_CUS" width="580" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <table align="center" bgcolor="#FFFFFF" width="580" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    ${checkData(sortedData, folderName)}
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    `;
    return html;
};

const checkData = (sortedData, folderName) => {
    let width = 0;

    let htmlString = "";
    _.forEach(sortedData, data => {
        let imageNum = data.num;
        if (imageNum < 10) imageNum = '0' + imageNum;
        const imageSrc = `./${folderName}/images/image_${imageNum}.jpg`
        const dimensions = sizeOf(imageSrc);
        width += dimensions.width;
        if (width >= 580) {
            width = 0;
            htmlString += `${buildTableData(data, dimensions, imageNum)}
            </tr> 
            </table>
            <table align="center" bgcolor="#FFFFFF" width="580" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    `
        }
        else {
            htmlString += `${buildTableData(data, dimensions, imageNum)}`
        }
    });
    return htmlString
}

const buildTableData = ({ alt, url }, dimensions, imageNum) => {
    if (!url)
        return `<td style="border-collapse:collapse;" valign="top"  > 
        <img style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0; margin: 0;" src="images/image_${imageNum}.jpg" width="${dimensions.width}" height="${dimensions.height}" alt="Footer">
        </td>
    `
    return `<td style="border-collapse:collapse;" valign="top"> 
                <a href="${url}">
                    <img style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block; border: 0; margin: 0;" src="images/image_${imageNum}.jpg" width="${dimensions.width}" height="${dimensions.height}" border="0" alt="${alt}">
                </a>
            </td>
        `
}

module.exports = { build };